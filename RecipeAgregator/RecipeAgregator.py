from db_manager import recipe_db_admin, recipe_db_user
import re
from recipe import recipe, recipe_db_filler
import sqlite3


db_handler = recipe_db_filler()

db_handler.parse_allrecipes_recipes()