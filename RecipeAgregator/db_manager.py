import sqlite3 as sql
import re
from copy import deepcopy
from urllib.request import urlopen, urlretrieve

class recipe_db_user:
    def __init__(self):
        self.connec = sql.connect("C:\Databases\Recipes_And_Ingredients.db")
        self.curs = self.connec.cursor()

    def print_table(self, name):
        if "recipes" in name:
            self.curs.execute("SELECT * FROM recipes")
        elif "recipes_steps" in name:
            self.curs.execute("SELECT * FROM recipes_ingredients")
        elif "recipes_ingredient_quantities" in name:
            self.curs.execute("SELECT * FROM recipes_ingredient_quantities")
        elif "ingredients" in name:
            self.curs.execute("SELECT * FROM ingredients")
        elif "ingredient_synonyms" in name:
            self.curs.execute("SELECT * FROM ingredient_synonyms")
        elif "ingredient_categories" in name:
            self.curs.execute("SELECT * FROM ingredient_categories")
        elif "ingredient_flavor_profiles" in name:
            self.curs.execute("SELECT * FROM ingredient_flavor_profiles")
        elif "flavor_profiles" in name:
            self.curs.execute("SELECT * FROM flavor_profiles")
        elif "flavor_profile_relationships" in name:
            self.curs.execute("SELECT * FROM flavor_profile_relationships")

        line = self.curs.fetchone()
        
        if line:
            print(line)

        while line:
            line = self.curs.fetchone()

            if line:
                print(line)

    def query_db(self, query):
        if re.search("DROP", query, re.IGNORECASE) or re.search("INSERT", query, re.IGNORECASE) or \
            re.search("DELETE", query, re.IGNORECASE) or re.search("TRUNCATE", query, re.IGNORECASE) or \
            re.search("MODIFY", query, re.IGNORECASE) or re.search("MERGE", query, re.IGNORECASE) or \
            re.search("ALTER", query, re.IGNORECASE) or re.search("ADD", query, re.IGNORECASE) or \
            re.search("ADMIN", query, re.IGNORECASE) or re.search("CONVERT", query, re.IGNORECASE) or \
            re.search("COPY", query, re.IGNORECASE) or re.search("CREATE", query, re.IGNORECASE) or \
            re.search("DESTROY", query, re.IGNORECASE) or re.search("DECLARE", query, re.IGNORECASE) or \
            re.search("CONNECT", query, re.IGNORECASE) or re.search("DISCONNECT", query, re.IGNORECASE) or \
            re.search("DECLARE", query, re.IGNORECASE) or re.search("LOCK", query, re.IGNORECASE) or \
            re.search("MOVE", query, re.IGNORECASE) or re.search("NONE", query, re.IGNORECASE) or \
            re.search("PURGE", query, re.IGNORECASE) or re.search("RENAME", query, re.IGNORECASE) or \
            re.search("REPLACE", query, re.IGNORECASE) or re.search("RESTART", query, re.IGNORECASE) or \
            re.search("RESET", query, re.IGNORECASE) or re.search("SHUTDOWN", query, re.IGNORECASE) or \
            re.search("UNDO", query, re.IGNORECASE) or re.search("UNLOCK", query, re.IGNORECASE) or \
            re.search("UPDATE", query, re.IGNORECASE) or re.search("ZEROFILL", query, re.IGNORECASE) or \
            re.search("--", query, re.IGNORECASE) or re.search("REVOKE", query, re.IGNORECASE) or \
            re.search("REVERT", query, re.IGNORECASE) or re.search("ROLLBACK", query, re.IGNORECASE):
            
            print("Toxic statements have been detected in the query. This connexion will now close.")
            
            self.connec.close()
            
            return None
        else:
            self.curs.execute(query)

            lines = []

            line = self.curs.fetchone()
        
            if line:
                lines.append(deepcopy(line))

            while line:
                line = self.curs.fetchone()

                if line:
                    lines.append(deepcopy(line))

            return lines

    def close(self):
        self.connec.close()

class recipe_db_handler(recipe_db_user):
    def expand_ingredient_db(self, ingredient_categories, ingredients, ingredient_synonyms, other_keywords):
        if ingredient_categories:
            self.curs.executemany("INSERT INTO ingredient_categories VALUES (?,?)", ingredient_categories)
        if ingredient_synonyms:
            self.curs.executemany("INSERT INTO ingredient_synonyms VALUES (?,?)", ingredient_synonyms)
        if ingredients:
            self.curs.executemany("INSERT INTO ingredients VALUES (?,?,?)", ingredients)
        if other_keywords:
            self.curs.executemany("INSERT INTO other_keywords VALUES (?)", other_keywords)
        self.connec.commit()

    def add_recipe(self,recipe_ID,recipe_URL,recipe_rating,max_rating,domain_name):
        self.curs.execute("INSERT INTO recipes VALUES (?,?,?,?,?)", (recipe_ID, recipe_URL, recipe_rating, max_rating,domain_name))

    def save(self):
        self.connec.commit()

    def save_and_close(self):
        self.connec.commit()
        self.connec.close()

    def remove_recipe_URL_duplicates(self):
        self.curs.execute("DELETE FROM recipes WHERE recipes.recipe_ID IN (SELECT T1.recipe_ID FROM recipes T1, recipes T2 WHERE T1.recipe_ID > T2.recipe_ID AND T1.recipe_URL = T2.recipe_URL)")
        self.save_and_close()

class recipe_db_admin(recipe_db_handler):
    def create_ingredient_db(self):
        self.curs.execute("CREATE TABLE recipes (recipe_ID INT NOT NULL PRIMARY KEY, recipe_URL CLOB(1000), recipe_rating REAL, max_rating REAL, domain CHAR(100))")
        self.curs.execute("CREATE TABLE recipes_steps (recipe_ID INT, step_number INT, step CHAR(200))")
        self.curs.execute("CREATE TABLE recipe_ingredient_quantities (recipe_ID INT, ingredient_ID INT, quantity CHAR(50))")
        self.curs.execute("CREATE TABLE ingredients (ingredient_ID INT NOT NULL PRIMARY KEY, ingredient_name CHAR(100), category_ID)")
        self.curs.execute("CREATE TABLE ingredient_synonyms (ingredient_ID INT, ingredient_synonym CHAR(100))")
        self.curs.execute("CREATE TABLE ingredient_categories (category_ID INT NOT NULL PRIMARY KEY, category_name CHAR(100))")
        self.curs.execute("CREATE TABLE ingredient_flavor_profiles (ingredient_ID INT, profile_ID INT)")
        self.curs.execute("CREATE TABLE flavor_profiles (profile_ID INT, profile_name CHAR(30))")
        self.curs.execute("CREATE TABLE flavor_profile_relationships (profile_ID1 INT, profile_ID2 INT, effect_of_1_on_2 INT)")
        self.curs.execute("CREATE TABLE other_keywords (keyword CHAR(30))")
        self.curs.execute("CREATE TABLE current_max_index (max_index INT)")
        self.curs.execute("CREATE TABLE freed_indices (freed_index INT)")
        self.curs.execute("CREATE TABLE current_fetch_keywords (fetch_keyword CHAR(100))")

        ingredient_categories = [
            (1, "spices and herbs"),
            (2, "fruits"),
            (3, "vegetables"),
            (4, "whole grains and starch"),
            (5, "grain derivatives"),
            (6, "nuts and seeds"),
            (7, "legumes"),
            (8, "legume derivatives"),
            (9, "animals"),
            (10, "animal derivatives"),
            (11, "beverage bases"),
            (12, "culinary fats")]

        ingredient_synonyms = [
                (21, "liquorice"),
                (25, "spearmint"),
                (25, "peppermint"),
                (57, "zucchini"),
                (75, "cantaloupe"),
                (75, "honeydew"),
                (75, "watermelon"),
                (82, "prune"),
                (89, "buttercup"),
                (89, "butternut"),
                (89, "pumpkin"),
                (93, "rocket"),
                (93, "roquette"),
                (97, "bok"),
                (110, "aubergine"),
                (117, "iceberg"),
                (117, "romaine"),
                (121, "okro"),
                (121, "ochro"),
                (142, "manioc"),
                (142, "tapioca"),
                (154, "agnolotti"),
                (154, "cannelloni"),
                (154, "farfalle"),
                (154, "fettuchine"),
                (154, "fusilli"),
                (154, "gnocchi"),
                (154, "linguine"),
                (154, "macaroni"),
                (154, "manicotti"),
                (154, "noodle"),
                (154, "orecchiette"),
                (154, "orzo"),
                (154, "penne"),
                (154, "pierogi"),
                (154, "ramen"),
                (154, "ravioli"),
                (154, "rigatoni"),
                (154, "rotini"),
                (154, "soba"),
                (154, "spaghetti"),
                (154, "spatzle"),
                (154, "tagliatelle"),
                (154, "tortellini"),
                (154, "udon"),
                (154, "vermicelli"),
                (154, "wonton"),
                (155, "anpan"),
                (155, "arepa"),
                (155, "bagel"),
                (155, "baguette"),
                (155, "bannock"),
                (155, "waffle"),
                (155, "bialy"),
                (155, "bing"),
                (155, "breadstick"),
                (155, "wafer"),
                (155, "ciabatta"),
                (155, "cornbread"),
                (155, "loaf"),
                (155, "cracker"),
                (155, "cookie"),
                (155, "crouton"),
                (155, "crepe"),
                (155, "crêpe"),
                (155, "crumpet"),
                (155, "dorayaki"),
                (155, "muffin"),
                (155, "flatbread"),
                (155, "focaccia"),
                (155, "fougasse"),
                (155, "pancake"),
                (155, "hardtack"),
                (155, "injera"),
                (155, "miche"),
                (155, "naan"),
                (155, "brioche"),
                (155, "panettone"),
                (155, "pita"),
                (155, "pretzel"),
                (155, "pumpernickel"),
                (155, "roti"),
                (155, "teacake"),
                (155, "tortilla"),
                (155, "biscuit"),
                (155, "brownie"),
                (155, "cake"),
                (155, "pastry"),
                (155, "pie"),
                (155, "viennoiserie"),
                (178, "edamame"),
                (178, "soybean"),
                (183, "anchovy"),
                (185, "calamari"),
                (185, "clam"),
                (185, "crab"),
                (185, "lobster"),
                (185, "mussel"),
                (185, "oyster"),
                (185, "scallop"),
                (185, "shrimp"),
                (185, "squid"),
                (183, "bass"),
                (183, "cod"),
                (183, "flounder"),
                (183, "halibut"),
                (183, "herring"),
                (183, "mackerel"),
                (183, "mahimahi"),
                (183, "perch"),
                (183, "pollock"),
                (183, "salmon"),
                (183, "snapper"),
                (183, "sturgeon"),
                (183, "tilapia"),
                (183, "turbot"),
                (183, "tuna"),
                (186, "lamb"),
                (184, "bacon"),
                (184, "pancetta"),
                (184, "ham"),
                (188, "blood"),
                (188, "breast"),
                (188, "leg"),
                (188, "liver"),
                (188, "loin"),
                (181, "picanha"),
                (188, "ribs"),
                (188, "shank"),
                (188, "sirloin"),
                (188, "steak"),
                (188, "tenderloin"),
                (188, "thigh"),
                (188, "wings"),
                (188, "feet"),
                (188, "fillet"),
                (188, "jerky"),
                (145, "oatmeal"),
                (184, "capicola"),
                (184, "chorizo"),
                (188, "salami"),
                (188, "sausage"),
                (188, "saucisson"),
                (188, "pastrami"),
                (184, "prosciutto"),
                (188, "pemmican"),
                (188, "pepperoni"),
                (193, "anejo"),
                (193, "beemster"),
                (193, "bocconcini"),
                (193, "brie"),
                (193, "burrata"),
                (193, "camembert"),
                (193, "cheddar"),
                (193, "chevrotin"),
                (193, "edam"),
                (193, "emmental"),
                (193, "feta"),
                (193, "gorgonzola"),
                (193, "gouda"),
                (193, "gruyere"),
                (193, "halloumi"),
                (193, "havarti"),
                (193, "jarlsberg"),
                (193, "limburger"),
                (193, "manchego"),
                (193, "mascarpone"),
                (193, "monterey"),
                (193, "mozzarella"),
                (193, "oka"),
                (193, "padano"),
                (193, "paneer"),
                (193, "parmesan"),
                (193, "pecorino"),
                (193, "provolone"),
                (193, "quark"),
                (193, "raclette"),
                (193, "reblochon"),
                (193, "rocamadour"),
                (193, "roquefort"),
                (193, "ricotta"),
                (193, "stilton"),
                (193, "queso"),
                (199, "vegetable oil"),
                (199, "canola oil"),
                (199, "olive oil"),
                (199, "sesame oil"),
                (199, "soybean oil"),
                (199, "corn oil"),
                (199, "rapeseed oil"),
                (199, "sunflower oil"),
                (199, "peanut oil"),
                (199, "coconut oil"),
                (199, "palm oil"),
                (199, "i can't believe it's not butter!"),
                (199, "avocado oil"),
                (199, "grape seed oil"),
                (199, "hazelnut oil"),
                (199, "linseed oil"),
                (200, "suet"),
                (200, "lard"),
                (200, "butter")]

        ingredients = [
                (1, "allspice", 1),
                (2, "anise", 1),
                (3, "basil", 1),
                (4, "bay", 1),
                (5, "caper", 1),
                (6, "caraway", 1),
                (7, "cardamom", 1),
                (8, "chives", 1),
                (9, "cilantro", 1),
                (10, "cinnamon", 1),
                (11, "coriander", 1),
                (12, "cress", 1),
                (13, "cumin", 1),
                (14, "dill", 1),
                (15, "fennel", 1),
                (16, "garlic", 1),
                (17, "horseradish", 1),
                (18, "juniper", 1),
                (19, "lavender", 1),
                (20, "lemongrass", 1),
                (21, "licorice", 1),
                (22, "mace", 1),
                (23, "majoram", 1),
                (24, "mastic", 1),
                (25, "mint", 1),
                (26, "mustard", 1),
                (27, "nutmeg", 1),
                (28, "oregano", 1),
                (29, "paprika", 1),
                (30, "parsley", 1),
                (31, "poppy", 1),
                (32, "rhubarb", 1),
                (33, "rose", 1),
                (34, "saffron", 1),
                (35, "sage", 1),
                (36, "tamarind", 1),
                (37, "tarragon", 1),
                (38, "thyme", 1),
                (39, "tumeric", 1),
                (40, "vanilla", 1),
                (41, "wasabi", 1),
                (42, "watercress", 1),
                (43, "wintergreen", 1),
                (44, "acai", 2),
                (45, "apple", 2),
                (46, "apricot", 2),
                (47, "banana", 2),
                (48, "blackcurrant", 2),
                (49, "blackberry", 2),
                (50, "blueberry", 2),
                (52, "carambola", 2),
                (53, "cherimoya", 2),
                (54, "cherry", 2),
                (55, "clementine", 2),
                (56, "cloudberry", 2),
                (57, "courgette", 2),
                (58, "cranberry", 2),
                (59, "currant", 2),
                (60, "date", 2),
                (61, "durian", 2),
                (62, "elderberry", 2),
                (63, "fig", 2),
                (64, "goji", 2),
                (65, "grape", 2),
                (66, "grapefruit", 2),
                (67, "guava", 2),
                (51, "jujube", 2),
                (68, "lemon", 2),
                (69, "lime", 2),
                (70, "lychee", 2),
                (71, "mandarin", 2),
                (72, "mango", 2),
                (73, "mangosteen", 2),
                (74, "marula", 2),
                (75, "melon", 2),
                (76, "nectarine", 2),
                (77, "orange", 2),
                (78, "papaya", 2),
                (79, "peach", 2),
                (80, "pear", 2),
                (81, "pineapple", 2),
                (82, "plum", 2),
                (83, "pomegranate", 2),
                (84, "pomelo", 2),
                (85, "raspberry", 2),
                (86, "soursop", 2),
                (87, "strawberry", 2),
                (88, "sweetsop", 2),
                (89, "squash", 2),
                (90, "tangerine", 2),
                (91, "artichoke", 3),
                (92, "avocado", 2),
                (93, "arugula", 3),
                (94, "asparagus", 3),
                (95, "bamboo", 3),
                (96, "beet", 3),
                (97, "bok choi", 3),
                (98, "broccoli", 3),
                (99, "broccolini", 3),
                (100, "cabbage", 3),
                (101, "carrot", 3),
                (102, "celery", 3),
                (103, "chard", 3),
                (104, "chayote", 3),
                (105, "chicory", 3),
                (106, "collard", 3),
                (107, "cucumber", 3),
                (108, "daikon", 3),
                (109, "dandelion", 3),
                (110, "eggplant", 3),
                (111, "endive", 3),
                (112, "fiddlehead", 3),
                (113, "kale", 3),
                (114, "kohlrabi", 3),
                (115, "kombu", 3),
                (116, "leek", 3),
                (117, "lettuce", 3),
                (118, "lotus", 3),
                (119, "napa", 3),
                (120, "nori", 3),
                (121, "okra", 3),
                (122, "olive", 3),
                (123, "onion", 3),
                (124, "parsnip", 3),
                (125, "radish", 3),
                (126, "rapini", 3),
                (127, "celeriac", 3),
                (128, "rutabaga", 3),
                (129, "scallion", 3),
                (130, "shallot", 3),
                (131, "sorrel", 3),
                (132, "spinach", 3),
                (133, "taro", 4),
                (134, "tomato", 2),
                (135, "turnip", 3),
                (136, "wakame", 3),
                (137, "yam", 3),
                (138, "amaranth", 4),
                (139, "barley", 4),
                (140, "buckwheat", 4),
                (142, "cassava", 4),
                (143, "corn", 4),
                (144, "millet", 4),
                (145, "oat", 4),
                (146, "potato", 4),
                (147, "quinoa", 4),
                (148, "rice", 4),
                (149, "rye", 4),
                (150, "sorghum", 4),
                (151, "spelt", 4),
                (152, "teff", 4),
                (153, "wheat", 4),
                (154, "pasta", 5),
                (155, "bread", 5),
                (156, "bulgur", 5),
                (157, "cereal", 5),
                (157, "couscous", 5),
                (158, "grits", 5),
                (159, "museli", 5),
                (160, "gruel", 5),
                (162, "almond", 6),
                (163, "cashew", 6),
                (164, "coconut", 6),
                (165, "hazelnut", 6),
                (166, "macadamia", 6),
                (167, "pecan", 6),
                (168, "pistachio", 6),
                (169, "walnut", 6),
                (170, "seitan", 5),
                (171, "bean", 7),
                (172, "chickpea", 7),
                (173, "lentil", 7),
                (174, "lupin", 7),
                (175, "pea", 7),
                (176, "peanut", 7),
                (177, "vetch", 7),
                (178, "soy", 7),
                (179, "tofu", 8),
                (180, "tempeh", 8),
                (181, "beef", 9),
                (182, "chicken", 9),
                (183, "fish", 9),
                (184, "pork", 9),
                (185, "seafood", 9),
                (186, "mutton", 9),
                (187, "goat", 9),
                (188, "meat", 9),
                (189, "quail", 9),
                (190, "egg", 10),
                (191, "honey", 10),
                (192, "milk", 10),
                (193, "cheese", 10),
                (194, "coffee", 11),
                (195, "tea", 11),
                (161, "beer", 11),
                (196, "wine", 11),
                (197, "liquor", 11),
                (198, "chocolate", 11),
                (199, "oil", 12),
                (200, "fat", 12)]

        other_keywords = [
                ("nachos",),
                ("pizza",),
                ("blue",),
                ("brazil",),
                ("brick",),
                ("brussels",),
                ("burger",),
                ("chestnut",),
                ("chili",),
                ("cottage",),
                ("cream",),
                ("curry",),
                ("flan",),
                ("hickory",),
                ("lasagna",),
                ("passion",),
                ("pepper",),
                ("pine",),
                ("savory",),
                ("shepherd",),
                ("swiss",),
                ("wild",)]

        self.curs.executemany("INSERT INTO ingredient_categories VALUES (?,?)", ingredient_categories)
        self.curs.executemany("INSERT INTO ingredient_synonyms VALUES (?,?)", ingredient_synonyms)
        self.curs.executemany("INSERT INTO ingredients VALUES (?,?,?)", ingredients)
        self.curs.executemany("INSERT INTO other_keywords VALUES (?)", other_keywords)
        self.curs.execute("INSERT INTO current_max_index VALUES (?)", (1,))
        self.connec.commit()

    def manage_database(self, statement):
        self.curs.execute(statement)