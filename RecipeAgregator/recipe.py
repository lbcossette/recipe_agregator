from db_manager import recipe_db_handler
from urllib.request import urlopen
import re

def cmp(x,y):
    try:
        diff = x-y
    except:
        return 0
    else:
        if diff > 0:
            return 1

        if diff == 0:
            return 0

        if diff < 0:
            return -1

class recipe:
    def __init__(self, url, rating):
        self.url = url
        self.rating = rating # Out of five
        self.ingredients_text = [] # Keys are ingredients and values are quantities in grams

class recipe_db_filler(recipe_db_handler):
    def fetch_allrecipes_recipes(self):
        keyword_entries = self.query_db("SELECT * from current_fetch_keywords")
        
        if not keyword_entries:
            keyword_entries = self.query_db("SELECT ingredient_name FROM ingredients")

            keyword_entries = keyword_entries + self.query_db("SELECT ingredient_synonym FROM ingredient_synonyms")

            keyword_entries = keyword_entries + self.query_db("SELECT keyword from other_keywords")

            self.curs.executemany("INSERT INTO current_fetch_keywords VALUES (?)", keyword_entries)
        else:
            print("Continuing from previous fetch attempt")

        current_recipe_ID = self.query_db("SELECT * FROM current_max_index")[0][0]

        for keyword_entry in keyword_entries:
            i = 0

            failure_counts = 0
    
            keyword = keyword_entry[0]

            while True:
                i += 1

                if failure_counts > 3:
                    break

                print("Page {:d} of keyword {:s}...".format(i, keyword))

                print("https://www.allrecipes.com/search/results/?wt={:s}&sort=re&page={:d}".format(keyword.lower(),i))

                try:
                    current_page = urlopen("https://www.allrecipes.com/search/results/?wt={:s}&sort=re&page={:d}".format(keyword.lower(),i))
                except:
                    print("ERROR")

                    break
                else:
                    current_page_content = current_page.readlines()

                    line = -1
                    create_new = False

                    page_success_count = 0

                    while line < len(current_page_content) - 1:
                        line += 1

                        if re.match(".*\<article class\=\"fixed\-recipe\-card\"\>", current_page_content[line].decode("utf-8")):
                            create_new = True

                        if create_new:
                            recipe_url = ""

                            recipe_rating = 0.0

                            num_rates = 0

                            while not re.match('^.*\<\/article\>', current_page_content[line].decode("utf-8")):
                                line += 1

                                if not recipe_url:
                                    line_search = re.search('(?<=\<a\shref\=\")[^\"]+', current_page_content[line].decode("utf-8"))

                                    if line_search and re.search('class\=\"fixed\-recipe\-card\_\_title\-link\"', current_page_content[line].decode("utf-8")):
                                        recipe_url = line_search.group(0)

                                elif recipe_rating == 0.0:
                                    line_search = re.search('(?<=data\-ratingstars\=\")[.\d]+', current_page_content[line].decode("utf-8"))

                                    if line_search:
                                        recipe_rating = float(line_search.group(0))

                                elif num_rates == 0:
                                    line_search = re.search('\<span class\=\"fixed\-recipe\-card\_\_reviews\"\>.*?number\=\"(\d+)', current_page_content[line].decode("utf-8"))

                                    if line_search:
                                        num_rates = int(line_search.group(1))
                
                            if num_rates > 30:
                                print("entry inserted into recipes : ({:d}, {:s}, {:f}, {:f}, {:s})".format(current_recipe_ID, recipe_url, recipe_rating, 5.0, "allrecipes"))
                        
                                self.add_recipe(current_recipe_ID, recipe_url, recipe_rating, 5.0, "allrecipes")

                                page_success_count += 1

                                current_recipe_ID += 1
                                self.curs.execute("UPDATE current_max_index SET max_index = max_index + 1")

                        create_new = False

                    if page_success_count == 0:
                        failure_counts += 1
                    else:
                        failure_counts = 0
            
            self.curs.execute("DELETE FROM current_fetch_keywords WHERE fetch_keyword = ?", keyword_entry)
            self.save()

    def parse_allrecipes_recipes(self):
        """
        To do : reorganize the order of ingredients so that those with the most words will come first
        """

        ingredients = dict() # Dictionary that associates ingredient text with ingredient id

        self.curs.execute("SELECT ingredients.ingredient_ID, ingredients.ingredient_name FROM ingredients")

        max_id = 0

        while True:
            current_ingredient = self.curs.fetchone()

            if not current_ingredient:
                max_id += 1
                break

            ingredients[current_ingredient[1].lower()] = current_ingredient[0]
            max_id = current_ingredient[0]

        self.curs.execute("SELECT * FROM ingredient_synonyms")

        while True:
            current_ingredient = self.curs.fetchone()

            if not current_ingredient:
                break

            ingredients[current_ingredient[1].lower()] = current_ingredient[0]
        
        self.curs.execute("SELECT * FROM ingredient_categories")

        categories = dict()

        max_category_id = 0

        while True:
            current_category = self.curs.fetchone()

            print(current_category)

            if not current_category:
                max_category_id += 1
                break

            categories[current_category[1].lower()] = current_category[0]

            max_category_id += 1

        self.curs.execute("SELECT recipes.recipe_ID, recipes.recipe_URL FROM recipes")

        all_recipes = self.curs.fetchall()

        step_id = 1

        print(categories)

        non_ingredients = []

        for current_recipe in all_recipes:
            if not "allrecipes.com" in current_recipe[1]:
                continue

            current_url = current_recipe[1]

            print("Parsing recipe {:s}".format(current_url))

            try:
                current_page = urlopen(current_url)
            except:
                print("Error opening {:s}".format(current_url))
            else:
                current_content = current_page.readlines()

                for j in range(len(current_content) - 2):

                    ingredient = re.search("title\=\"([\w\s\/\.\,\(\)\-\&\%\#\;\*\!]+)", current_content[j].decode("utf-8"))

                    if ingredient and re.search("name\=\"ingredientCheckbox\"", current_content[j+1].decode("utf-8")):
                        text = ingredient.group(1)
                
                        text = re.sub("&#39;", "\'", text)

                        text_ingredient = ""

                        noningredient = False

                        ingredient_words = list(ingredients.keys())

                        ingredient_words = sorted(ingredient_words, key=len, reverse=True)

                        for i in ingredient_words:
                            if i.lower() in text.lower():
                                if i == "tea" and "teaspoon" in text.lower(): # TO BE CONTINUED
                                    continue
                                text_ingredient = i.lower()
                                print("Found ingredient name {:s} in text {:s}".format(i, text.lower()))
                                break

                        for i in non_ingredients:
                            if i.lower() in text.lower():
                                noningredient = True
                                break

                        if noningredient:
                            continue

                        try:
                            text_id = ingredients[text_ingredient]
                        except:
                            print("New ingredient text found : {:s}".format(text.lower()))

                            if "yes" in input("Is it an ingredient? (yes/no): ").lower():
                                if "yes" in input("Is it an existing ingredient? (yes/no): ").lower():
                                    if "yes" in input("Is it an existing ingredient synonym? (yes/no): ").lower():
                                        while True:
                                            try:
                                                text_ingredient = input("Please type the name of the synonym: ").lower()

                                                text_id = ingredients[text_ingredient]
                                            except:
                                                print("This is not an existing ingredient")
                                            else:
                                                break
                                    else:
                                        while True:
                                            try:
                                                actual_ingredient = input("Please type the name of the associated ingredient: ").lower()

                                                text_id = ingredients[actual_ingredient]
                                            except:
                                                print("This is not an existing ingredient")
                                            else:
                                                text_ingredient = input("Please type the name of the new synonym: ").lower()
                                                
                                                self.curs.execute("INSERT INTO ingredient_synonyms VALUES (?,?)",[text_id, text_ingredient])

                                                ingredients[text_ingredient] = text_id
                                                break
                                else:
                                    while True:
                                        try:
                                            actual_category = input("Please type the name of the ingredient category: ").lower()

                                            category_id = categories[actual_category]
                                        except:
                                            print("This is not an existing category")
                                        else:
                                            text_ingredient = input("Please type the name of the new ingredient: ").lower()

                                            self.curs.execute("INSERT INTO ingredients VALUES (?,?,?)",[max_id, text_ingredient, category_id])

                                            synonym_text = input("Please type the ingredient synonym or \"None_\" if this is not a synonym: ").lower()

                                            if not "none_" in synonym_text:
                                                ingredients[synonym_text] = max_id

                                                self.curs.execute("INSERT INTO ingredient_synonyms VALUES (?,?)",[max_id, synonym_text])

                                            ingredients[text_ingredient] = max_id

                                            print(ingredients)

                                            text_id = max_id
                                            max_id += 1
                                            break
                            elif "yes" in input("Is it in a new ingredient category? (yes/no): ").lower():
                                category_text = input("Please type the name of the new category: ")
                                category_id = max_category_id

                                text_ingredient = input("Please type the name of the new ingredient: ")
                                text_id = max_id

                                self.curs.execute("INSERT INTO ingredient_categories VALUES (?,?)", [category_id, category_text])
                                self.curs.execute("INSERT INTO ingredients VALUES (?,?,?)", [text_id, text_ingredient, category_id])

                                ingredients[text_ingredient] = text_id
                                categories[category_text] = category_id

                            else:
                                non_ingredients.append(text.lower())

                        self.curs.execute("INSERT INTO recipe_ingredient_quantities VALUES (?,?,?)", [current_recipe[0], text_id, re.sub(text_ingredient, "", text.lower())])

                    recipe_step = re.search("\<li class\=\"step\" ng\-class\=\"\{\'finished\'\: stepIsActive", current_content[j].decode("utf-8"))

                    if recipe_step:
                        step_content = current_content[j+1].decode("utf-8")

                        step = re.sub("[\s\t]+\<span class\=\"recipe\-directions\_\_list\-\-item\"\>","",step_content)

                        self.curs.execute("INSERT INTO recipe_steps VALUES (?,?,?)", [current_recipe[0], step_id, step])
            self.save()